# init

possible_datapoints= [[7.7, 2.6, 6.9, 2.3],[5.0, 3.4,1.6,0.4], [6.7,3.3,5.7,2.1],[4.8,3.1,1.6,0.2],[5.1,3.3,1.7,0.5],[6.8,3.2,5.9,2.3],[6.5,3.0,5.5,1.8],[5.5,2.3,4.0,1.3],[4.4,3.0,1.3,0.2],[4.6,3.2,1.4,0.2]]
cores =  1#:erlang.system_info(:schedulers_online)

datapoints = Enum.map(1..100_000, fn _ -> Enum.random(possible_datapoints) end)

{:ok, graph} = Tensorflex.read_graph("trained_iris_graphdef.pb")

# actual benchmark

Benchee.run(
  %{
    "batch_100_000" => fn -> ModelServer.predict_class(graph, datapoints) end,
   },
  time: 10
)