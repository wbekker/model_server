defmodule Router do
  use Plug.Router

  plug(:match)
  plug(:dispatch)

  get "/" do
    conn
    |> put_resp_content_type("application/json")
    |> send_resp(200, Poison.encode!(message()))
  end

  get "/:datapoints" do
    # TODO: move graph load to a router init function
    # TODO: get datapoints via a json struct (so we can have multiple inputs)

    conn = case conn.assigns[:graph] do
      nil ->
        {:ok, graph} = Tensorflex.read_graph("trained_iris_graphdef.pb")
        assign(conn, :graph, graph)
      _ ->
        conn
    end

    datapoints_list = [_sepal_length, _sepal_width, _petal_length, _petal_width] = String.split(datapoints, ",")

    datapoints_list = datapoints_list |> Enum.map(fn datapoint -> {result, _} = Float.parse(datapoint); result end)

    [class] = ModelServer.predict_class(conn.assigns[:graph], [datapoints_list])

    send_resp(conn, 200, "class predicated: #{class}")
  end

  defp message do
    %{
      response_type: "in_channel",
      text: "Hello from BOT :)"
    }
  end
end