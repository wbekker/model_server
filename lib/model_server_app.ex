defmodule ModelServerApp do
  @moduledoc false

  use Application

  def start(_type, _args) do
    Supervisor.start_link(children(), opts())
  end

  defp opts do
    [
      strategy: :one_for_one,
      name: Supervisor
    ]
  end

  defp children do
    [
      Endpoint
    ]
  end
end