defmodule ModelServer do
  @moduledoc """
  Documentation for ModelServer.
  """

  @doc """
  Hello world.

  ## Examples

      iex> ModelServer.hello()
      :world

  """

  def start_experiment(batch_size) do

    # step 1. serve a machine learning model with an api
    # https://www.tensorflow.org/install/lang_c
    # sudo tar -C /usr/local/lib -xzf  ~/Downloads/libtensorflow.tar
    # pip3 install tensorflow
    # pip3 install pandas
    # git clone https://github.com/anshuman23/tensorflex.git for iris example lib generation
    #    Attribute Information:
    #    sepal length in cm
    #    sepal width in cm
    #    petal length in cm
    #    petal width in cm
    #  class:
    #  Iris Setosa
    #  Iris Versicolour
    #  Iris Virginica
    # For datapoint: [7.7 2.6 6.9 2.3] Actual: [0. 0. 1.] Predicted: [[0. 0. 1.]]
    #For datapoint: [5.  3.4 1.6 0.4] Actual: [1. 0. 0.] Predicted: [[1. 0. 0.]]
    #For datapoint: [6.7 3.3 5.7 2.1] Actual: [0. 0. 1.] Predicted: [[0. 0. 1.]]
    #For datapoint: [4.8 3.1 1.6 0.2] Actual: [1. 0. 0.] Predicted: [[1. 0. 0.]]
    #For datapoint: [5.1 3.3 1.7 0.5] Actual: [1. 0. 0.] Predicted: [[1. 0. 0.]]
    #For datapoint: [6.8 3.2 5.9 2.3] Actual: [0. 0. 1.] Predicted: [[0. 0. 1.]]
    #For datapoint: [6.5 3.  5.5 1.8] Actual: [0. 0. 1.] Predicted: [[0. 0. 1.]]
    #For datapoint: [5.5 2.3 4.  1.3] Actual: [0. 1. 0.] Predicted: [[0. 1. 0.]]
    #For datapoint: [4.4 3.  1.3 0.2] Actual: [1. 0. 0.] Predicted: [[1. 0. 0.]]
    #For datapoint: [4.6 3.2 1.4 0.2] Actual: [1. 0. 0.] Predicted: [[1. 0. 0.]]

    #    {:ok, graph} = Tensorflex.read_graph("trained_iris_graphdef.pb")
    #
    #    in_vals = Tensorflex.create_matrix(10,4,[[7.7, 2.6, 6.9, 2.3],[5.0, 3.4,1.6,0.4], [6.7,3.3,5.7,2.1],[4.8,3.1,1.6,0.2],[5.1,3.3,1.7,0.5],[6.8,3.2,5.9,2.3],[6.5,3.0,5.5,1.8],[5.5,2.3,4.0,1.3],[4.4,3.0,1.3,0.2],[4.6,3.2,1.4,0.2]])
    #    in_dims = Tensorflex.create_matrix(1,2,[[10,4]])
    #    {:ok, input_tensor} = Tensorflex.float32_tensor(in_vals, in_dims)
    #    out_dims = Tensorflex.create_matrix(1,2,[[10,3]])
    #    {:ok, output_tensor} = Tensorflex.float32_tensor_alloc(out_dims)
    #    Tensorflex.run_session(graph, input_tensor, output_tensor, "input","output")

    # TODO:
    # - simple rest API
    # - create first benchmarks (https://github.com/dwyatte/tensorflow-serving-benchmark?)
    # - https://medium.com/tensorflow/serving-ml-quickly-with-tensorflow-serving-and-docker-7df7094aa008
    # - https://www.usenix.org/system/files/conference/nsdi17/nsdi17-crankshaw.pdf
    # - pretty graphs


    :noop
  end

  def predict_class(graph, datapoints) do
    in_vals = Tensorflex.create_matrix(length(datapoints), 4, datapoints)
    in_dims = Tensorflex.create_matrix(1, 2, [[length(datapoints), 4]])
    {:ok, input_tensor} = Tensorflex.float32_tensor(in_vals, in_dims)
    out_dims = Tensorflex.create_matrix(1, 2, [[length(datapoints), 3]])
    {:ok, output_tensor} = Tensorflex.float32_tensor_alloc(out_dims)
    results = Tensorflex.run_session(graph, input_tensor, output_tensor, "input", "output")

    Enum.map(
      results,
      fn [setosa_prediction, versicolour_prediction, virginica_prediction] ->
        iris_class(setosa_prediction, versicolour_prediction, virginica_prediction)
      end
    )

  end


  def iris_class(setosa_prediction, versicolour_prediction, virginica_prediction) do
    setosa = Kernel.round(setosa_prediction)
    versicolour = Kernel.round(versicolour_prediction)
    virginica = Kernel.round(virginica_prediction)

    case [setosa, versicolour, virginica] do
      [1, 0, 0] ->
        :setosa
      [0, 1, 0] ->
        :versicolour
      [0, 0, 1] ->
        :virginica
      _ ->
        :no_predication
    end
  end
end
