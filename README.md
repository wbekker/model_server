# ModelServer

- Free open source Tensorflow online model serving a la Travis
- Highly performance / availability
- REST API (maybe gRCP)
- Easy CLI script for easy publishing of graph file.
- Simple visualisation of model usage / performance?

## CLI

- Publish new model
- Add new version of model 

## Limitations

- Probably fixed on versions
- Model size max 100 MB (https://medium.com/google-cloud/optimizing-tensorflow-models-for-serving-959080e9ddbf)
- Currently online predications. Perhaps some batch predication API's in the future. 

## Other considerations

- Tensorflow version compatibility
- Security of running 3rd party code?


# Marketing

10 M QPS+. Iris, perhaps also  

